package com.jokenpo.usecase

import com.jokenpo.data.Play

class JokenpoService {

    fun play(playerMove: String) : Play {
        val opponentMove = playAdversary()
        if (playerMove == STONE && opponentMove == SCISSORS){
            return Play(SCISSORS, playerMove,1)
        } else if (playerMove == STONE && opponentMove == PAPER){
            return Play(PAPER, playerMove,0)
        } else if (playerMove == SCISSORS && opponentMove == PAPER){
            return Play(PAPER, playerMove,1)
        } else if (playerMove == SCISSORS && opponentMove == STONE){
            return Play(STONE, playerMove, 0)
        } else if (playerMove == PAPER && opponentMove == SCISSORS){
            return Play(SCISSORS, playerMove, 0)
        } else if (playerMove == PAPER && opponentMove == STONE){
            return Play(STONE, playerMove, 1)
        }
        return Play(opponentMove, playerMove,2)
    }

    fun playAdversary() : String {
        val opponentMove = when((1..3).random()){
            1 -> STONE
            2 -> PAPER
            else -> SCISSORS
        }
        return opponentMove
    }

    companion object {
        const val STONE = "stone"
        const val PAPER = "paper"
        const val SCISSORS = "scissors"
    }
}
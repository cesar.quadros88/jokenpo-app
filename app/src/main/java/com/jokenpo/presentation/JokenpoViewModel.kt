package com.jokenpo.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jokenpo.data.Play
import com.jokenpo.usecase.JokenpoService

class JokenpoViewModel : ViewModel(){

    val playLiveData: MutableLiveData<Play> = MutableLiveData()

    fun play(playerMove: String) {
        val service = JokenpoService()
        playLiveData.value = service.play(playerMove)
    }
}
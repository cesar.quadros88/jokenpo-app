package com.jokenpo.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.ViewModelProvider
import com.jokenpo.R
import com.jokenpo.data.Play
import com.jokenpo.databinding.ActivityMainBinding
import com.jokenpo.usecase.JokenpoService
import com.jokenpo.usecase.JokenpoService.Companion.PAPER
import com.jokenpo.usecase.JokenpoService.Companion.SCISSORS
import com.jokenpo.usecase.JokenpoService.Companion.STONE

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    lateinit var service: JokenpoService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        val viewModel: JokenpoViewModel = ViewModelProvider(this).get(JokenpoViewModel::class.java)

        viewModel.playLiveData.observe(this) { play ->
            binding.txtResult.text = if (play.result == 1) "Venceu" else if (play.result == 0 )"Perdeu" else "Empatou"
            setImage(play.opponentMove, binding.imgOponent)
            setImage(play.playerMove, binding.imgPlayer)
        }

        binding.imgStone.setOnClickListener{
            viewModel.play(STONE)
        }
        binding.imgPaper.setOnClickListener{
            viewModel.play(PAPER)
        }
        binding.imgScissors.setOnClickListener{
            viewModel.play(SCISSORS)
        }
    }
    fun setImage(move: String, imgView: ImageView){
        when(move){
            STONE -> imgView.setImageResource(R.drawable.pedra)
            PAPER -> imgView.setImageResource(R.drawable.papel)
            SCISSORS -> imgView.setImageResource(R.drawable.tesoura)
        }
    }
}